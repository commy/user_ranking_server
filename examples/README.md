## 管理者個人の結果メモ置き場

正当性は未検証 (vectorで検証する予定)

#### CPU
* Intel(R) Core(TM) i5-7Y54 CPU @ 1.20GHz   1.60 GHz
* 検証時は大体2.0GHz~2.4GHzくらいで動作している
* たまに0.9GHzまで悪化するのでその時はあてにしてはいけない

### skiplist.cpp
* skiplist+mapによる実装
```
$ ./skiplist.exe 123456
[info] seed = 123456
simulation 1 done (6153 msec)
simulation 2 done (7025 msec)
simulation 3 done (7533 msec)
simulation 4 done (7356 msec)
simulation 5 done (7369 msec)
simulation 6 done (7329 msec)
simulation 7 done (7287 msec)
simulation 8 done (7348 msec)
simulation 9 done (7305 msec)
simulation 10 done (7317 msec)
[elapsed time (10 times average)]: 7202.2 msec (388.93 msec)
validation_hash: 38ba0d01a4443c
```
