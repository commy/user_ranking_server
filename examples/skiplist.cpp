#include <iostream>
#include <chrono>
#include <vector>
#include <random>
#include <algorithm>
#include <iomanip>

#include <utility>
#include <random>
#include <map>

struct UserData {
    unsigned int ranking;
    unsigned long long score;
    unsigned int id;
    UserData() : UserData(0, 0, 0) {}
    UserData(unsigned int r, unsigned long long s, unsigned int i) : ranking(r), score(s), id(i) {}
    unsigned long long hash() {
        return ((static_cast<unsigned long long>(id) << 32) | ranking) ^ score;
    }
};

namespace ranking_server {
    template<class T, class Compare = std::less<T>>
    class skiplist {
        static constexpr int MAXLEVEL = 15;
        class Node {
            T value;
            std::vector<Node *> next;
            std::vector<int> interval;
            friend skiplist;

        public:
            template<class F>
            Node(F &&val, int level) : value(std::forward<F>(val)), next(level + 1, nullptr), interval(level + 1, 0) {}
        };
        class NodeIterator {
            Node *iterator;

        public:
            NodeIterator(Node *itr) : iterator(itr) {}
            NodeIterator operator++() {
                this->iterator = this->iterator->next[0];
                return *this;
            }
            T &operator*() {
                return this->iterator->value;
            }
            bool operator!=(const NodeIterator &node) {
                return this->iterator != node.iterator;
            }
        };

        Node *head;
        std::mt19937 mt;
        int sz;
        std::uniform_real_distribution<double> frand;

        int choose_level() {
            constexpr double p = 0.5;
            double pp = 1;
            double r = frand(mt);
            for (int i = 0; i < skiplist::MAXLEVEL; i++) {
                pp *= p;
                if (r >= pp) return i;
            }
            return skiplist::MAXLEVEL;
        }

        std::pair<std::vector<Node *>, std::vector<int>> search(T &value) {
            Node *cur = this->head;
            std::vector<Node *> prev(skiplist::MAXLEVEL + 1, nullptr);
            std::vector<int> idx(skiplist::MAXLEVEL + 1, 0);
            for (int i = skiplist::MAXLEVEL; i >= 0; i--) {
                while (cur->next[i] != nullptr && Compare()(cur->next[i]->value, value)) {
                    idx[i] += cur->interval[i];
                    cur = cur->next[i];
                }
                prev[i] = cur;
            }
            return make_pair(move(prev), move(idx));
        }

    public:
        skiplist() : head(new Node(T(), skiplist::MAXLEVEL)), mt((std::random_device())()), sz(0), frand(0, 1) {}
        template<class F>
        void insert(F &&value) {
            auto [prev, idx] = search(value);
            if (prev[0]->next[0] == nullptr || Compare()(prev[0]->next[0]->value, value) || Compare()(value, prev[0]->next[0]->value)) {
                int level = this->choose_level();
                int total_idx = 0, partial_idx = 0;
                for (const auto &i : idx) {
                    total_idx += i;
                }
                for (int i = level + 1; i <= skiplist::MAXLEVEL; i++) {
                    partial_idx += idx[i];
                    prev[i]->interval[i]++;
                }
                this->sz++;
                Node *cur = new Node(std::forward<F>(value), level);
                for (int i = level; i >= 0; i--) {
                    partial_idx += idx[i];
                    cur->interval[i] = partial_idx + prev[i]->interval[i] - total_idx;
                    cur->next[i] = prev[i]->next[i];
                    prev[i]->interval[i] = total_idx - partial_idx + 1;
                    prev[i]->next[i] = cur;
                }
            }
        }
        template<class F>
        void erase(F &&value) {
            auto [prev, _] = search(value);
            Node *cur = prev[0]->next[0];
            if (cur != nullptr && !Compare()(cur->value, value) && !Compare()(value, cur->value)) {
                for (unsigned int i = skiplist::MAXLEVEL; i >= cur->interval.size(); i--) {
                    prev[i]->interval[i]--;
                }
                for (unsigned int i = 0; i < cur->next.size(); i++) {
                    prev[i]->interval[i] += cur->interval[i] - 1;
                    prev[i]->next[i] = cur->next[i];
                }
                delete cur;
                this->sz--;
            }
        }
        void erase_by_index(int idx) {
            T value = (*this)[idx];
            erase(move(value));
        }
        template<class F>
        std::pair<int, bool> get_index(F &&value) {
            auto [prev, idx] = search(value);
            int total_idx = 0;
            for (const auto &i : idx) {
                total_idx += i;
            }
            Node *cur = prev[0]->next[0];
            return std::make_pair(total_idx, cur != nullptr && !Compare()(cur->value, value) && !Compare()(value, cur->value));
        }
        T &operator[](int idx) {
            Node *cur = this->head;
            idx += 1;
            for (int i = skiplist::MAXLEVEL; i >= 0; i--) {
                while (cur != nullptr && cur->interval[i] < idx) {
                    idx -= cur->interval[i];
                    cur = cur->next[i];
                }
            }
            return cur->next[0]->value;
        }
        int size() {
            return this->sz;
        }
        NodeIterator begin() {
            return NodeIterator(this->head->next[0]);
        }
        NodeIterator end() {
            return NodeIterator(nullptr);
        }
        template<class F>
        NodeIterator value_iterator(F &&value) {
            auto [prev, _] = search(value);
            return NodeIterator(prev[0]->next[0]);
        }
        ~skiplist() {
            while (this->head != nullptr) {
                Node *nxt = this->head->next[0];
                delete this->head;
                this->head = nxt;
            }
        }
        void reset() {
            while (this->head != nullptr) {
                Node *nxt = this->head->next[0];
                delete this->head;
                this->head = nxt;
            }
            this->sz = 0;
            this->head = new Node(T(), skiplist::MAXLEVEL);
        }
    };

    struct user {
        unsigned int id;
        unsigned long long point;
        user() : user(0, 0) {}
        user(unsigned int _id, unsigned long long _point) : id(_id), point(_point) {}
        bool operator>(const user &u) const {
            return this->point != u.point ? this->point > u.point : this->id < u.id;
        }
    };

    skiplist<user, std::greater<user>> sl;
    std::map<unsigned int, unsigned long long> mp;

    // リクエスト開始前に呼ばれる前準備
    void setup(const std::vector<unsigned int> &ids) {
        for (auto id : ids) {
            sl.insert(user(id, 0));
            mp[id] = 0;
        }
    }

    // API．`id`に`incr_point`足すリクエストを処理
    void add_point(unsigned int id, unsigned long long incr_point) {
        auto it = mp.find(id);
        user key(id, it->second);
        sl.erase(key);
        key.point += incr_point;
        sl.insert(key);
        mp[id] += incr_point;
    }

    // API． user `id` の51人(前後25人ずつ+id)を取得(同点数の場合はidが若い順)
    std::vector<UserData> get_user_ranking(unsigned int id) {
        user key(id, mp[id]);
        auto [rank, _] = sl.get_index(key);
        rank = std::max(0, rank - 25);
        user u = sl[rank];
        auto it = sl.value_iterator(u);
        std::vector<UserData> res;
        res.reserve(51);
        for (int i = 0; i < 51; i++, ++it) {
            if (it != sl.end()) {
                res.emplace_back(rank + i + 1, (*it).point, (*it).id);
            } else {
                break;
            }
        }
        return res;
    }

    // API．ランキングTOP50を取得(同点数の場合はidが若い順)
    std::vector<UserData> get_top_ranking() {
        std::vector<UserData> res;
        res.reserve(50);
        auto it = sl.begin();
        for (int i = 0; i < 50; i++, ++it) {
            if (it != sl.end()) {
                res.emplace_back(i + 1, (*it).point, (*it).id);
            } else {
                break;
            }
        }
        return res;
    }

    // 後処理．
    void teardown() {
        sl.reset();
        mp.clear();
    }
};  // namespace ranking_server

int main(int argc, char **argv) {
    std::cin.tie(nullptr);
    std::ios::sync_with_stdio(false);

    constexpr int simulation_max = 10;
    constexpr int user_max = 100'000;
    constexpr int request_max = 1'000'000;
    constexpr double high_point_rate = 0.1;
    constexpr int user_ranking_interval = 10;
    constexpr int top_ranking_interval = 20;

    std::mt19937 mt;
    if (argc > 1) {
        int seed = std::atoi(argv[1]);
        mt = std::mt19937(seed);
        std::cerr << "[info] seed = " << seed << std::endl;
    } else {
        mt = std::mt19937((std::random_device())());
    }

    std::uniform_int_distribution<unsigned int> user_id_generator(1, 10'000'000 - 1);
    std::uniform_int_distribution<unsigned int> user_selector(0, user_max - 1);
    std::uniform_int_distribution<unsigned long long> point_generator_low(1, 100);
    std::uniform_int_distribution<unsigned long long> point_generator_high(1, 10'000'000);

    // system setup
    std::vector<unsigned int> ids(user_max);
    for (auto &id : ids) {
        id = user_id_generator(mt);
    }
    std::vector<unsigned long long> point(request_max);
    std::vector<unsigned int> point_user(request_max);
    for (int i = 0; i < request_max; i++) {
        point[i] = point_generator_low(mt);
        point_user[i] = ids[user_selector(mt)];
    }
    for (int i = 0; i < static_cast<int>(request_max * high_point_rate); i++) {
        point[i] = point_generator_high(mt);
    }
    std::shuffle(begin(point), end(point), mt);

    std::vector<long double> elapsed_times(simulation_max);
    unsigned long long validation_hash = 0;
    for (int simulation_count = 0; simulation_count < simulation_max; simulation_count++) {
        auto start = std::chrono::high_resolution_clock::now();
        ranking_server::setup(ids);
        for (int i = 0; i < request_max; i++) {
            ranking_server::add_point(point_user[i], point[i]);
            if ((i + 1) % user_ranking_interval == 0) {
                auto user_ranking = ranking_server::get_user_ranking(point_user[i]);
                // std::cout << "[user ranking]" << point_user[i] << std::endl;
                for (auto &&user : user_ranking) {
                    validation_hash ^= user.hash();
                    // std::cout << user.ranking << ", " << user.score << ", " << user.id << std::endl;
                }
            }
            if ((i + 1) % top_ranking_interval == 0) {
                auto top_ranking = ranking_server::get_top_ranking();
                // std::cout << "[top ranking]" << std::endl;
                for (auto &&user : top_ranking) {
                    validation_hash ^= user.hash();
                    // std::cout << user.ranking << ", " << user.score << ", " << user.id << std::endl;
                }
            }
        }
        ranking_server::teardown();

        auto finish = std::chrono::high_resolution_clock::now();
        elapsed_times[simulation_count] = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count();
        std::cout << "simulation " << simulation_count + 1 << " done (" << elapsed_times[simulation_count] << " msec)" << std::endl;

        std::shuffle(begin(point), end(point), mt);
        std::shuffle(begin(point_user), end(point_user), mt);
    }
    long double mean = std::accumulate(begin(elapsed_times), end(elapsed_times), 0.0) / size(elapsed_times);
    long double sd = std::sqrt((std::inner_product(begin(elapsed_times), end(elapsed_times), begin(elapsed_times), 0.0) - mean * mean * size(elapsed_times)) / (std::size(elapsed_times) - 1));
    std::cout << "[elapsed time (" << simulation_max << " times average)]: " << mean << " msec (" << sd << " msec)" << std::endl;
    std::cout << "validation_hash: " << std::hex << validation_hash << std::endl;
}
