#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>

struct UserData {
    unsigned int ranking;
    unsigned long long score;
    unsigned int id;
    UserData() : UserData(0, 0, 0) {}
    UserData(unsigned int r, unsigned long long s, unsigned int i) : ranking(r), score(s), id(i) {}
    unsigned long long hash() {
        return ((static_cast<unsigned long long>(id) << 32) | ranking) ^ score;
    }
};

namespace ranking_server {
    // リクエスト開始前に呼ばれる前準備
    void setup(const std::vector<unsigned int> &ids) {}

    // API．`id`に`incr_point`足すリクエストを処理
    void add_point(unsigned int id, unsigned long long incr_point) {}

    // API． user `id` の51人(前後25人ずつ+id)を取得(同点数の場合はidが若い順)
    std::vector<UserData> get_user_ranking(unsigned int id) {}

    // API．ランキングTOP50を取得(同点数の場合はidが若い順)
    std::vector<UserData> get_top_ranking() {}

    // 後処理．
    void teardown() {}
};  // namespace ranking_server

int main(int argc, char **argv) {
    std::cin.tie(nullptr);
    std::ios::sync_with_stdio(false);

    constexpr int simulation_max = 10;
    constexpr int user_max = 100'000;
    constexpr int request_max = 1'000'000;
    constexpr double high_point_rate = 0.1;
    constexpr int user_ranking_interval = 10;
    constexpr int top_ranking_interval = 20;

    std::mt19937 mt;
    if (argc > 1) {
        int seed = std::atoi(argv[1]);
        mt = std::mt19937(seed);
        std::cerr << "[info] seed = " << seed << std::endl;
    } else {
        mt = std::mt19937((std::random_device())());
    }

    std::uniform_int_distribution<unsigned int> user_id_generator(1, 10'000'000 - 1);
    std::uniform_int_distribution<unsigned int> user_selector(0, user_max - 1);
    std::uniform_int_distribution<unsigned long long> point_generator_low(1, 100);
    std::uniform_int_distribution<unsigned long long> point_generator_high(1, 10'000'000);

    // system setup
    std::vector<unsigned int> ids(user_max);
    for (auto &id : ids) {
        id = user_id_generator(mt);
    }
    std::vector<unsigned long long> point(request_max);
    std::vector<unsigned int> point_user(request_max);
    for (int i = 0; i < request_max; i++) {
        point[i] = point_generator_low(mt);
        point_user[i] = ids[user_selector(mt)];
    }
    for (int i = 0; i < static_cast<int>(request_max * high_point_rate); i++) {
        point[i] = point_generator_high(mt);
    }
    std::shuffle(begin(point), end(point), mt);

    std::vector<long double> elapsed_times(simulation_max);
    unsigned long long validation_hash = 0;
    for (int simulation_count = 0; simulation_count < simulation_max; simulation_count++) {
        auto start = std::chrono::high_resolution_clock::now();
        ranking_server::setup(ids);
        for (int i = 0; i < request_max; i++) {
            ranking_server::add_point(point_user[i], point[i]);
            if ((i + 1) % user_ranking_interval == 0) {
                auto user_ranking = ranking_server::get_user_ranking(point_user[i]);
                for (auto &&user : user_ranking) {
                    validation_hash ^= user.hash();
                }
            }
            if ((i + 1) % top_ranking_interval == 0) {
                auto top_ranking = ranking_server::get_top_ranking();
                for (auto &&user : top_ranking) {
                    validation_hash ^= user.hash();
                }
            }
        }
        ranking_server::teardown();

        auto finish = std::chrono::high_resolution_clock::now();
        elapsed_times[simulation_count] = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count();
        std::cout << "simulation " << simulation_count + 1 << " done (" << elapsed_times[simulation_count] << " msec)" << std::endl;

        std::shuffle(begin(point), end(point), mt);
        std::shuffle(begin(point_user), end(point_user), mt);
    }
    long double mean = std::accumulate(begin(elapsed_times), end(elapsed_times), 0.0) / size(elapsed_times);
    long double sd = std::sqrt((std::inner_product(begin(elapsed_times), end(elapsed_times), begin(elapsed_times), 0.0) - mean * mean * size(elapsed_times)) / (std::size(elapsed_times) - 1));
    std::cout << "[elapsed time (" << simulation_max << " times average)]: " << mean << " msec (" << sd << " msec)" << std::endl;
    std::cout << "validation_hash: " << std::hex << validation_hash << std::endl;
}
